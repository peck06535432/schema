const express = require('express')
const router = express.Router()
const ShowdocsController = require('../controller/ShowdocsController')

/* GET users listing. */
router.get('/', ShowdocsController.getDocs)

router.get('/:id', ShowdocsController.getDoc)

module.exports = router
