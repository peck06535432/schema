const express = require('express')
const router = express.Router()
const companiesController = require('../controller/CompaniesController')
const studentController = require('../controller/StudentsController')

/* GET users listing. */
router.get('/', companiesController.getCompanies)

router.get('/student', studentController.getStudents)

router.get('/:id', companiesController.getCompany)

router.get('/student/:id', studentController.getStudent)

router.post('/', companiesController.addCompany)

router.post('/student/', studentController.addStudent)

router.put('/', companiesController.updateCompany)

router.put('/student/', studentController.updateStudent)

router.delete('/:id', companiesController.deleteCompany)

router.delete('/student/:id', studentController.deleteStudent)

module.exports = router
