const express = require('express')
const router = express.Router()
const docsController = require('../controller/DocsController')

/* GET users listing. */
router.get('/', docsController.getDocs)

router.get('/:id', docsController.getDoc)

router.post('/', docsController.addDoc)

router.put('/', docsController.updateDoc)

router.delete('/:id', docsController.deleteDoc)
module.exports = router
