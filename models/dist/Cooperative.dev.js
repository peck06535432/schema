"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var cooperativeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  detail: {
    type: String,
    required: true
  },
  lists: [{
    keyId: {
      type: String,
      minlength: 8,
      maxlength: 8,
      required: true
    },
    lname: {
      type: String,
      minlength: 3,
      required: true
    },
    status: {
      type: Boolean,
      "enum": [true, false, null],
      required: true
    }
  }]
});
module.exports = mongoose.model('cooperatives', cooperativeSchema);