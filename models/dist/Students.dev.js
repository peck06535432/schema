"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var studentSchema = new Schema({
  studentId: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 8
  },
  // รหัสนิสิต
  password: {
    type: String,
    minlength: 8,
    required: true
  },
  // รหัสผ่าน
  name: {
    type: String,
    required: true
  },
  // ชื่อ
  // firstName: String, // ชื่อ
  // lastName: String, // นามสกุล
  faculty: {
    type: String,
    required: true
  },
  // คณะ
  branch: {
    type: String,
    "enum": ['CS', 'IT', 'SE']
  },
  // สาขา
  gpax: {
    type: Number,
    required: true,
    min: 0,
    max: 4
  },
  // เกรดเฉลี่ย
  cooperativeStatus: {
    type: Boolean,
    required: true
  },
  // สถานะสหกิจ (พร้อมหรือไม่)
  advisor: {
    type: String,
    required: true
  },
  // อาจารย์ที่ปรึกษา
  company: {
    type: String,
    required: true
  },
  // บริษัท (ที่เข้าศึกษาสหกิจ)
  role: {
    type: String,
    required: true
  } // ตำแหน่ง (ที่เข้าศึกษาสหกิจ)

});
module.exports = mongoose.model('Student', studentSchema);