const mongoose = require('mongoose')
const Schema = mongoose.Schema
const studentSchema = new Schema({
  studentId: String, // รหัสนิสิต
  password: String, // รหัสผ่าน
  name: String, // ชื่อ
  faculty: String, // คณะ
  branch: String, // สาขา
  gpax: Number, // เกรดเฉลี่ย
  cooperativeStatus: Boolean, // สถานะสหกิจว่ากำลังฝึกงานอยู่
  advisor: String, // อาจารย์ที่ปรึกษา
  company: String, // บริษัท (ที่เข้าศึกษาสหกิจ)
  role: String, // ตำแหน่ง (ที่เข้าศึกษาสหกิจ)
  educateTime: {
    academicTime: Number, // ชั่วโมงอบรม วิชาการ
    prepareTime: Number // ชั่วโมงอบรม เตรียมความพร้อม
  },
  registCooperativeStatus: Boolean, // สมัครสหกิจไปรึยัง?
  subject: [
    {
      88510059: {
        name: String,
        grade: Number
      },
      88510259: {
        name: String,
        grade: Number
      },
      88510459: {
        name: String,
        grade: Number
      },
      88620159: {
        name: String,
        grade: Number
      },
      88620259: {
        name: String,
        grade: Number
      },
      88620359: {
        name: String,
        grade: Number
      },
      88620459: {
        name: String,
        grade: Number
      },
      88621159: {
        name: String,
        grade: Number
      },
      88634159: {
        name: String,
        grade: Number
      },
      88666659: {
        name: String,
        grade: Number
      }
    }
  ]
})

module.exports = mongoose.model('Student', studentSchema)
