const dbhandler = require('./db-handler')
const Companies = require('../models/Companies')

beforeAll(async () => {
  await dbhandler.connect()
})

afterEach(async () => {
  await dbhandler.clearDatabase()
})

afterAll(async () => {
  await dbhandler.closeDatabase()
})

const companiesComplete1 = {
  name: 'samsung',
  phone: '0882233996',
  email: 'samsung@gmail.com',
  address: 'ศูนย์บริการลูกค้าสัมพันธ์ (Customer Contact Center)'
}

const companiesError1 = {
  name: '',
  phone: '',
  email: '',
  address: ''
}

const companiesError2 = {
  name: '',
  phone: '0268932777',
  email: 'samsung@gmail.com',
  address: 'ศูนย์บริการลูกค้าสัมพันธ์ (Customer Contact Center)'
}

const companiesError3 = {
  name: 'samsung',
  phone: '',
  email: 'samsung@gmail.com',
  address: 'ศูนย์บริการลูกค้าสัมพันธ์ (Customer Contact Center)'
}

const companiesError4 = {
  name: 'samsung',
  phone: '0268932777',
  email: '',
  address: 'ศูนย์บริการลูกค้าสัมพันธ์ (Customer Contact Center)'
}

const companiesError5 = {
  name: 'samsung',
  phone: '0268932777',
  email: 'samsung@gmail.com',
  address: ''
}

const companiesError6 = {
  name: 's',
  phone: '026893277',
  email: 'samsung@gmail.com',
  address: 'ศูนย์บริการลูกค้าสัมพันธ์ (Customer Contact Center)'
}

describe('Companies', () => {
  //  ---------------------------------------------สามารถเพิ่มสถานประกอบการ
  it('สามารถเพิ่มสถานประกอบการ ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesComplete1)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(ข้อมูลว่างทั้งหมด)
  it('ไม่สามารถสถานประกอบการ(ข้อมูลว่างทั้งหมด) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError1)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(ชื่อว่าง)
  it('ไม่สามารถสถานประกอบการ(ชื่อว่าง) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError2)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(เบอร์ว่าง)
  it('ไม่สามารถสถานประกอบการ(เบอร์ว่าง) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError3)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(อีเมลว่าง)
  it('ไม่สามารถสถานประกอบการ(อีเมลว่าง) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError4)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(เบอร์ต้องมี 10 เลข)
  it('ไม่สามารถสถานประกอบการ(เบอร์ต้องมี 10 เลข) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError5)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  //  ---------------------------------------------ไม่สามารถสถานประกอบการ(ชื่ออย่างน้อง 2 ตัว)
  it('ไม่สามารถสถานประกอบการ(ชื่ออย่างน้อง 2 ตัว) ', async () => {
    let error = null
    try {
      const companies = new Companies(companiesError6)
      await companies.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
