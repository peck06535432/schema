"use strict";

var dbhandler = require('./db-handler');

var Cooperative = require('../models/Cooperative');

beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbhandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbhandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbhandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
});
var cooperativeComplete = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
};
var cooperativeIncomplete1 = {
  name: '',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
};
var cooperativeIncomplete2 = {
  name: 'TEST',
  date: null,
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
};
var cooperativeIncomplete3 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: '',
  lists: []
};
var cooperativeIncomplete4 = {
  name: '',
  date: null,
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
};
var cooperativeIncomplete5 = {
  name: 'TEST',
  date: null,
  address: 'Bangkok',
  detail: '',
  lists: []
};
var cooperativeIncomplete6 = {
  name: '',
  date: new Date(),
  address: 'Bangkok',
  detail: '',
  lists: []
};
var cooperativeIncomplete7 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: '123456',
  lists: []
};
var cooperativeIncomplete8 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Test',
  lists: []
};
var cooperativeIncomplete9 = {
  name: '',
  date: null,
  address: 'Bangkok',
  detail: '',
  lists: []
};
describe('Cooperative', function () {
  it('สามารถเพิ่มการประกาศได้ ', function _callee4() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            error = null;
            _context4.prev = 1;
            cooperative = new Cooperative(cooperativeComplete);
            _context4.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            error = _context4.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท', function _callee5() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            error = null;
            _context5.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete1);
            _context5.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            error = _context5.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีวันที่ปิดรับสมัคร', function _callee6() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            error = null;
            _context6.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete2);
            _context6.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            error = _context6.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีข้อมูลรายละเอียด', function _callee7() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            error = null;
            _context7.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete3);
            _context7.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context7.next = 10;
            break;

          case 7:
            _context7.prev = 7;
            _context7.t0 = _context7["catch"](1);
            error = _context7.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ วันที่ปิดรับสมัคร', function _callee8() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            error = null;
            _context8.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete4);
            _context8.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context8.next = 10;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](1);
            error = _context8.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีวันที่ปิดรับสมัคร และ ข้อมูลรายละเอียด', function _callee9() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            error = null;
            _context9.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete5);
            _context9.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context9.next = 10;
            break;

          case 7:
            _context9.prev = 7;
            _context9.t0 = _context9["catch"](1);
            error = _context9.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ ข้อมูลรายละเอียด', function _callee10() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            error = null;
            _context10.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete6);
            _context10.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context10.next = 10;
            break;

          case 7:
            _context10.prev = 7;
            _context10.t0 = _context10["catch"](1);
            error = _context10.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ข้อมูลรายละเอียดต้องมีมากกว่า 6 ตัวอักษร', function _callee11() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            error = null;
            _context11.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete7);
            _context11.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context11.next = 10;
            break;

          case 7:
            _context11.prev = 7;
            _context11.t0 = _context11["catch"](1);
            error = _context11.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context11.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ข้อมูลรายละเอียดต้องมีมากกว่า 6 ตัวอักษร', function _callee12() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            error = null;
            _context12.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete8);
            _context12.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context12.next = 10;
            break;

          case 7:
            _context12.prev = 7;
            _context12.t0 = _context12["catch"](1);
            error = _context12.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context12.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ วันที่ปิดรับสมัคร และ ข้อมูลรายละเอียด', function _callee13() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            error = null;
            _context13.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete9);
            _context13.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context13.next = 10;
            break;

          case 7:
            _context13.prev = 7;
            _context13.t0 = _context13["catch"](1);
            error = _context13.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context13.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
});