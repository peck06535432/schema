const dbhandler = require('./db-handler')
const Cooperative = require('../models/Cooperative')

beforeAll(async () => {
  await dbhandler.connect()
})

afterEach(async () => {
  await dbhandler.clearDatabase()
})

afterAll(async () => {
  await dbhandler.closeDatabase()
})

const cooperativeComplete = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [
    { keyId: '60160270', lname: 'Supawee Test1', status: true },
    { keyId: '57160270', lname: 'Supawee Test2', status: false }
  ]
}

const cooperativeComplete2 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{ keyId: '1234567', lname: 'HEY', status: true }]
}

const cooperativeIncomplete = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [
    { keyId: '1234567', lname: 'Supawee Test1', status: true },
    { keyId: '123456789', lname: 'Supawee Test2', status: false }
  ]
}

const cooperativeIncomplete2 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [
    { keyId: '1234567', lname: 'Su', status: true },
    { keyId: '123456789', lname: '', status: false }
  ]
}

const cooperativeIncomplete3 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [
    {
      keyId: '60160270'
    }
  ]
}
const cooperativeIncomplete4 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{ keyId: '123456789', lname: '', status: 'NOPE' }]
}

describe('Cooperative', () => {
  it('สามารถเพิ่มรายชื่อได้ ', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeComplete)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่มรายชื่อได้ ชื่อนิสิต เท่ากับ 3 ตัวอักษร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeComplete2)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มรายชื่อได้ ไม่ได้ใส่ข้อมูลชื่อนิสิต และสถานะนิสิต', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete3)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มรายชื่อได้ รหัสนิสิตน้อยกว่าหรือมากกว่า 8 ตัวอักษร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มรายชื่อได้ ชื่อนิสิต น้อยกว่า 3 ตัวอักษร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete2)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มรายชื่อได้ สถานะนิสิต ไม่อยู่ใน True False Null', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete4)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
